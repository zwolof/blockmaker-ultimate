public Plugin:myinfo = { name = "The BlockMaker: Ultimate", author = "diablix", description = "", version = "0.1", url = "none atm" }

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <blockmakerultimate_blocks>
#include "BMUlt\blocks\blockdata.sp"
#include "BMUlt\blocks\stocks.sp"
#include "BMUlt\blocks\blocktypes.sp"
#include "BMUlt\blocks\utils.sp"
const MaxBlocks=1536;

BM_BlockDataSet g_Block[MaxBlocks];
GlobalForward g_BM_BlockTouchedForward;

public _hook_TouchBlock(int iBlock, int iClient){
	if(!(1 <= iClient <= MaxClients) || g_Block[iBlock].BlockType < view_as<blockTypes>(1)) return 0;
	
	Call_StartForward(g_BM_BlockTouchedForward);
	Call_PushCell(iBlock);
	Call_PushCell(iClient);
	Call_PushCell(g_Block[iBlock].BlockType);
	Call_Finish();
	
	return 0;
}

public Action _hookSay(int iClient, int iNoArgs){
	char sOutput[128];
	GetCmdArgString(sOutput, sizeof sOutput);
	StripQuotes(sOutput);

	if(StrEqual(sOutput, "!bhop")|| StrEqual(sOutput, "!platform")){
		int iNewEntity = CreateEntityByName("prop_physics_override");
		if(iNewEntity!=-1){
			BM_BlockDataSet newBlock;
	
			newBlock.Create(iClient, (StrEqual(sOutput, "!bhop") ? BLOCK_BHOP : BLOCK_PLATFORM), BLOCK_SIZE_NORMAL, iNewEntity);
		}
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

public OnMapStart(){
	blockTypes_createArray();
	blockTypes_precacheArray();
	
	for(int i=0 ;i<MaxBlocks; i++) g_Block[i].ResetData();
}

public APLRes AskPluginLoad2(Handle hMyself, bool bLate, char[] sError, int iErrMax){
	CreateNative("BM_Register_BlockType", _nativeBlockTypeRegister);
	return APLRes:0;
}

public void OnPluginStart(){
	g_BM_BlockTouchedForward = new GlobalForward("BM_OnBlockTouch", ExecType:0x03, ParamType:(1<<1), ParamType:(1<<1), ParamType:(0x0));

	RegConsoleCmd("say", _hookSay);
}

public Action OnPlayerRunCmd(int iClient, &Buttons, &Impulse, float fVel[3], float fAng[3], &Wep){
	if(Buttons&IN_RELOAD){
		int iEnt = GetClientAimTarget(iClient, false);
		if(IsValidEntity(iEnt)){
			PrintToChatAll("Is ent");
			if(g_Block[iEnt].BlockType>BLOCK_NONE){
				PrintToChatAll("TESTING ID BLOCKTYPE = %d", g_Block[iEnt].BlockType);
				PrintToChatAll("NO ERROR");
			}
			else PrintToChatAll("ERROR#1 - BlockType Bounds Error");
		}
	}
}