
#if defined _blockmakerultimate_inc_included
 #endinput
#endif
#define _blockmakerultimate_inc_included

forward void BM_OnBlockTouch(int iBlock, int iClient, blockTypes BlockType);
native BM_Register_BlockType(int iBlockType, char[] sBlockName, char[] sPath);