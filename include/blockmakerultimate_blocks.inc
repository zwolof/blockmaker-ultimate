
#if defined _blockmakerultimate_blocks_included
 #endinput
#endif
#define _blockmakerultimate_blocks_included

enum blockTypes{
	BLOCK_NONE			= 0x00001,
	BLOCK_PLATFORM		= 0x00002,
	BLOCK_BHOP   		= 0x00004,

    BLOCK_MAX			= 3
};