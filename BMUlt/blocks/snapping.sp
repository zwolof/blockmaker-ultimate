



float fPossible[3], float fMinsToCheck[3], float fMaxsToCheck[3];
fMinsToCheck = {-15.97, -16.00, -3.97};
fMaxsToCheck = {15.97, 16.00, 3.97};

float fPossOri[3]
Handle hTrace = TR_TraceHullFilterEx(fPossibleOri, fPossibleOri, fMinsToCheck, fMaxsToCheck, CONTENTS_SOLID, _trOnlyOtherBlocks, iBlockGrabbed);

public bool _trOnlyOtherBlocks(iBlockGrabbed, iBitMask, any:iData) { return (iBlockGrabbed != iData/* and isvalid block ent later..*/); }
/*
Where θ is the angle of rotation

X = ori[0];
Y = ori[1];

degree to rotate θ = 90
x′=xcosθ−ysinθ
y′=ycosθ+xsinθ
*/