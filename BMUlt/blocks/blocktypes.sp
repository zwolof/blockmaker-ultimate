
ArrayList g_ArrayBlockTypes, g_ArrayBlockTypesNames, g_ArrayBlockTypesPaths;
char g_sBlockResType[6][] = {".mdl", ".vvd", ".phy", ".dx90.vtx", ".vmt", ".vtf" };

void blockTypes_precacheArray(){
	char sPath[128];
	for(int i=0 ; i < g_ArrayBlockTypes.Length ; i++){
		g_ArrayBlockTypesPaths.GetString(i, sPath, sizeof sPath);
		LogMessage(sPath);
		AddFileToDownloadsTable(sPath);
		PrecacheModel(sPath);
		for(int k = 1 ; k < 4 ; k++){ //1=.vvd 4 = .vmt
			ReplaceString(sPath, sizeof sPath, g_sBlockResType[k], g_sBlockResType[k+1]);
			AddFileToDownloadsTable(sPath);
		}
		ReplaceString(sPath, sizeof sPath, "models", "materials");
		for(int m = 4 ; m < 6 ; m++){ //6 = max resource types
			ReplaceString(sPath, sizeof sPath, g_sBlockResType[m], g_sBlockResType[m+1]);
			AddFileToDownloadsTable(sPath);
		}	//W.I.P _s (SIDES MATERIALS)
	}
}

void blockTypes_registerToArray(blockTypes BlockType, char[] sBlock, char[] sPath){
	if(g_ArrayBlockTypes.Length >= _:BLOCK_MAX){
		LogError("Block type adding error: %s (%d)", sBlock, _:BlockType);
		LogError("Path: %s", sPath);
		return;
	}
	
	g_ArrayBlockTypes.Push(_:BlockType);
	g_ArrayBlockTypesNames.PushString(sBlock);
	g_ArrayBlockTypesPaths.PushString(sPath);
	
	LogMessage("Block %s (%s) added - %d", sBlock, sPath, _:BlockType);
}

void blockTypes_getModelPathByBlockType(blockTypes blockType, char[] sFormatPath){
	int iSize = GetArraySize(g_ArrayBlockTypes);
	if(iSize>=1){
		for(int i = 0 ; i<iSize ; i++){
			if(GetArrayCell(g_ArrayBlockTypes, i)==view_as<int>(blockType)){
				int iLen=strlen(sFormatPath)+1;
				g_ArrayBlockTypesPaths.GetString(i, sFormatPath, iLen);
				PrintToChatAll("%d = %s", i, sFormatPath);
				return;
			}
		}
	}
	sFormatPath[0]=EOS;
}

void blockTypes_createArray(){
	g_ArrayBlockTypes = new ArrayList();
	g_ArrayBlockTypesNames = new ArrayList(96, 0);
	g_ArrayBlockTypesPaths = new ArrayList(96, 0);
}