const float TempBlockPlaceDistance = 150.0;

void util_AttachToAimed(int iClient, int iToApply, char[] sMdlPath){
	float fPos[3];
	GetAimOriginDist(iClient, fPos, 100.0);
	
	DispatchKeyValue(iToApply, "model", sMdlPath);
	TeleportEntity(iToApply, fPos, view_as<float>({0.0, 0.0, 0.0}), NULL_VECTOR);
	DispatchSpawn(iToApply);
	SetEntityMoveType(iToApply, MOVETYPE_NONE);
	
	SDKHook(iToApply, SDKHook_Touch, _hook_TouchBlock);
}

public int _nativeBlockTypeRegister(Handle hPlugin, int iParamNum){
	int iType = GetNativeCell(1), iLen;
	
	GetNativeStringLength(2, iLen); iLen++;
	char[] sBlockName = new char[iLen];
	GetNativeString(2, sBlockName, iLen);
	
	GetNativeStringLength(3, iLen); iLen++;
	char[] sBlockPath = new char[iLen];
	GetNativeString(3, sBlockPath, iLen);
	blockTypes_registerToArray(view_as<blockTypes>(iType), sBlockName, sBlockPath);
}