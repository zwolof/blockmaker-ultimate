
enum struct BM_BlocksRotateData{
	int iRotatingEnt;
	float fAngStarting[3];
	
	void reset(){
		this.iRotatingEnt=-1;
		this.fAngStarting=view_as<float>({0.0, 0.0, 0.0});
	}
	void Block_StartedRotating(int iBlock){
		this.iRotatingEnt = iBlock;
	}
	void Block_RotateByAim(int iClient){
		float fAngles[3], fOffset[3];
		GetClientEyeAngles(iClient, fAngles);
		
		if(!Math_VectorsEqual(this.fAngStarting, fAngles, 0.0)){
			int i;
			for(i = 0 ; i < 3 ; i++) fOffset[i]=this.fAngStarting[i]-fAngles[i];
			Entity_GetAbsAngles(this.iRotatingEnt, fAngles);
			for(i = 0 ; i < 2 ; i++)
				fAngles[i+1]+=fOffset[i==0?1:0]*9.0;
				
			Entity_SetAbsAngles(this.iRotatingEnt, fAngles);
			GetClientEyeAngles(iClient, this.fAngStarting);
		}
	}
}
BM_BlocksPhysicsData rotateData[MAXPLAYERS+1];

void _Block_StartRotate(int iClient, int iBlock){
	SetEntityMoveType(iBlock, MOVETYPE_NONE);
	SetEntProp(iBlock, Prop_Send, "m_nSolidType", SOLID_VPHYSICS);
	
	PrintToChat(iClient, "Use your mouse to rotate the block");
	GetClientEyeAngles(iClient, rotateData[iClient].fAngStarting);
	rotateData[iClient].Block_StartedRotating(iBlock);
}

void _Block_StopRotate(int iClient){
	rotateData[iClient].iRotatingEnt=0;
	PrintToChat(iClient, "DEBUG: END Rotate hook");
}