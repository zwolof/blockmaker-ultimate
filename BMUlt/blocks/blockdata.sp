enum blockSizes{
	BLOCK_SIZE_POLE,
	BLOCK_SIZE_SMALL,
	BLOCK_SIZE_NORMAL,
	BLOCK_SIZE_LARGE
};

enum struct BM_BlockDataSet{
	bool IsBlock;
	char s_BaseMdlPath[256];
	blockTypes BlockType;
	blockSizes BlockSize;
	
	blockTypes Create(int iClient, blockTypes createdBlockType, blockSizes createdBlockSize, int iPreSpawnedEntity){
		this.ResetData();
		this.BlockType|=createdBlockType;
		this.BlockSize=createdBlockSize;
		
		blockTypes_getModelPathByBlockType(createdBlockType, this.s_BaseMdlPath);
		util_AttachToAimed(iClient, iPreSpawnedEntity, this.s_BaseMdlPath);
		
		PrintToChatAll("SPAWNED %d (sum-%d)(size %d)(%s)", createdBlockType, this.BlockType, this.BlockSize, this.s_BaseMdlPath);
		return this.BlockType;
	}
	blockTypes Remove(int toRemove){
		AcceptEntityInput(toRemove, "Kill");
		this.ResetData();
		this.IsBlock=false;
	}
	blockTypes Convert(blockTypes blockTypeToConvert){
		blockTypes oldType = this.BlockType;
		this.BlockType=(this.BlockType^this.BlockType)|blockTypeToConvert;
		PrintToChatAll("CONVERTED %d into %d(sum-%d)", oldType, blockTypeToConvert, this.BlockType);
		return this.BlockType;
	}
	blockTypes ScaleEffects(blockTypes blockTypeToScale){
		this.BlockType|=blockTypeToScale;
		PrintToChatAll("SCALED %d (sum-%d)", blockTypeToScale, this.BlockType);
		return this.BlockType;
	}
	blockTypes IsolateEffects(blockTypes blockTypeToIsolate){
		this.BlockType=this.BlockType^blockTypeToIsolate;
		return this.BlockType;
	}
	blockTypes ResetData(){
		this.BlockType&=(~((BLOCK_NONE<<BLOCK_MAX)-BLOCK_NONE));
		return this.BlockType;
	}
}