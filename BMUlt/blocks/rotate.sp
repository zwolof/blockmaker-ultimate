
enum struct BM_RotatePhysicsData{
	int iRotatingEnt;
	float fAngStarting[3];
	
	void reset(){
		this.iRotatingEnt=-1;
		this.fAngStarting=view_as<float>({0.0, 0.0, 0.0});
	}
	void Block_StartedRotating(int iBlock, float fAng[3]){
		for(int i=0;i<3;i++)
			this.fAngStarting[i] = fAng[i];
		this.iRotatingEnt = iBlock;
	}
	void Block_RotateByAim(int iClient){
		if(this.iRotatingEnt!=-1){ // not rly needed
			float fAngles[3], fOffset[3];
			GetClientEyeAngles(iClient, fAngles);
			
			if(!Math_VectorsEqual(this.fAngStarting, fAngles, 0.0)){
				int i;
				for(i = 0 ; i < 3 ; i++) fOffset[i]=this.fAngStarting[i]-fAngles[i];
				Entity_GetAbsAngles(this.iRotatingEnt, fAngles);
				for(i = 0 ; i < 2 ; i++)
					fAngles[i+1]+=fOffset[i==0?1:0]*9.0;
					
				Entity_SetAbsAngles(this.iRotatingEnt, fAngles);
				GetClientEyeAngles(iClient, this.fAngStarting);
			}
		}
	}
	void Block_EndRotating(){
		this.iRotatingEnt = 0;
	}
}
BM_RotatePhysicsData 	g_RotateData[MAXPLAYERS+1];

void _Block_StartRotate(int iClient, int iBlock){
	float fActualAng[3];
	GetClientEyeAngles(iClient, fActualAng);
	SetEntityMoveType(iBlock, MOVETYPE_NONE);
	SetEntProp(iBlock, Prop_Send, "m_nSolidType", SOLID_VPHYSICS);
	
	g_RotateData[iClient].Block_StartedRotating(iBlock, fActualAng);
	PrintToChat(iClient, "Use your mouse to rotate the block");
}

void _Block_StopRotate(int iClient){
	g_RotateData[iClient].Block_EndRotating();
	PrintToChat(iClient, "DEBUG: END Rotate hook");
}