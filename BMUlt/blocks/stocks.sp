//temp.
int GetAimOriginDist(iClient, Float:hOrigin[3], Float:fDist=100.0) {
	new Float:vAngles[3], Float:fOrigin[3], Float:fFw[3];
	GetClientEyePosition(iClient,fOrigin);
	GetClientEyeAngles(iClient, vAngles);
	GetAngleVectors(vAngles, fFw, NULL_VECTOR, NULL_VECTOR);
	NormalizeVector(fFw, fFw);
	ScaleVector(fFw, fDist);
	
	AddVectors(hOrigin, fOrigin, hOrigin);
	AddVectors(hOrigin, fFw, hOrigin);

	Handle hTrace = TR_TraceRayFilterEx(fOrigin, hOrigin, MASK_SHOT, RayType_EndPoint, trNoPlayers);
	if(TR_DidHit(hTrace)){
		int iFound;
		TR_GetEndPosition(hOrigin, hTrace);
		CloseHandle(hTrace);
		return IsValidEntity(iFound=TR_GetEntityIndex(hTrace)) ? iFound : 1;
	}

	CloseHandle(hTrace);
	return -1;
}

//for later to organize
bool trNoPlayers(int iEnt, int iBitMask, any iData){ return !(1<=iEnt<=MaxClients); }