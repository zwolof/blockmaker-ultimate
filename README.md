## BlockMaker Ultimate 
---
The BlockMaker is created by [diablix](https://steamcommunity.com/id/dramenbejs/) and [zwolof](https://steamcommunity.com/id/qvqv/) from scratch
The plugin comes pre-packed with models and block-modules and texture
templates for your own custom blocks.
## Features:
- Dynamic Blocks
- Module based Code for readability
- Created by EasyBlock Professionals
- Comes with pre-made blocks
- I like turtles

---
# Requirements
- SourceMod latest stable
- CS:GO Server
- Time
---