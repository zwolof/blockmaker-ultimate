public Plugin:myinfo = { name = "The BlockMaker: BunnyHop Block", author = "diablix", description = "", version = "0.1", url = "" }

#include <sourcemod>
#include <sdktools>

#include <blockmakerultimate_blocks>
#include <blockmakerultimate_inc>

public void OnPluginStart(){
	BM_Register_BlockType(BLOCK_BHOP, "Bunnyhop", "models/playcore2020/normal/bunnyhop.mdl");
}

public void BM_OnBlockTouch(int iBlock, int iClient, blockTypes BlockType){
	if(BlockType&BLOCK_BHOP){
		PrintToChatAll("Bunnyhop Action Touch");
	}
}